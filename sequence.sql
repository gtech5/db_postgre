-- public.seq_tb_member definition

-- DROP SEQUENCE public.seq_tb_member;

CREATE SEQUENCE public.seq_tb_member
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

-- Permissions

ALTER SEQUENCE public.seq_tb_member OWNER TO adminrza;
GRANT ALL ON SEQUENCE public.seq_tb_member TO adminrza;