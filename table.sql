-- public.tb_member definition

-- Drop table

-- DROP TABLE public.tb_member;

CREATE TABLE public.tb_member (
	tbm_id int4 NOT NULL DEFAULT nextval('seq_tb_member'::regclass),
	tbm_first_name varchar NULL,
	tbm_last_name varchar NULL,
	tbm_gender varchar NULL,
	tbm_date_of_birth date NULL,
	tbm_mobile_phone varchar NULL,
	tbm_email varchar NULL,
	CONSTRAINT tb_member_pkey PRIMARY KEY (tbm_id)
);

-- Permissions

ALTER TABLE public.tb_member OWNER TO adminrza;
GRANT ALL ON TABLE public.tb_member TO adminrza;